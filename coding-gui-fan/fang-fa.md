# 方法

在方法的命名規則中，我們區分為兩種，分別是有攜帶參數的，和不需要參數的。

### 未攜帶參數的方法

**第一個單字首字小寫**，第二個單字首字大寫的駝峰式規範，如下範例

```objectivec
//取的Banner圖片
-(void)getBannerImage{
     ...
}
```

### 攜帶參數的方法

**第一個單字首字小寫**，第二個單字首字大寫的駝峰式規範，後方盡量參數依重要性質排列，並用With銜接，如下方範例兩個參數分別為routeID和city，routeID為主要的參數所以在前面，參數首字也都皆為大寫，如下範例

```objectivec
//取得站牌資料
-(void)getStopDataWithRouteID:(NSString *)routeID City:(NSString*)city{
    ...
}
```

{% hint style="info" %}
&#x20;請注意方法名稱都是由動詞＋名詞組合而成，請明確並且簡單扼要的命名，必要的話上方可加上註解。
{% endhint %}
