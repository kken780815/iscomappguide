# 介面元件的宣告

介面的命名規則中，**第一個單字首字小寫，後方單字字首為大寫，最後加上型別**

```objectivec
@interface MoreTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UIImageView *picImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;
@end
```

{% hint style="info" %}
&#x20;名稱通常都用名詞組合而成，後方型別也可用縮寫，但請明確並且簡單扼要的命名，必要的話後面可加上註解。
{% endhint %}

