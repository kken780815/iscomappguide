# 變數

在變數的命名規則我們是用**第一個單字首字小寫**，第二個單字首字大寫的駝峰式規範，如下範例

```objectivec
@interface Push_VC (){
    int selectIdx; //選擇的項目
    NSString *logStr;
    BOOL isReading;
}
```

{% hint style="info" %}
&#x20;名稱通常都是名詞組合而成，請明確並且簡單扼要的命名，必要的話後面可加上註解，這邊特別注意Bool值都用is開頭，其餘後面可以加上型別。
{% endhint %}

