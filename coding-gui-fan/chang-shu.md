# 常數

在常數的命名規則我們是用**首字加上小寫k，表示const常數**，如下範例

```objectivec
static const NSString *kWeachatUUIDString = @"FDA50693-A4E2-4FB1-AFCF-C6EB07647825"; //beacon設備的ID
```

{% hint style="info" %}
名稱通常都是名詞組合而成，請明確並且簡單扼要的命名，必要的話後面可加上註解。
{% endhint %}
