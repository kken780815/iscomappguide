# 定義

在寫程式中，我們可能會想把常用的字串或者數值，儲存起來以供或其他人使用，這時候就可以使用定義#defin，通常我們會將他定義在專案裡生成的Header.pch文件裡面，命名方式單字全都大寫，並且使用底線隔開，如下範例

```objectivec
#define TAB_COUNT 2
#define TOP_HEIGHT 60
#define SCREEN_WIDTH  [UIScreen mainScreen].bounds.size.width
#define SCREEN_HEIGHT  [UIScreen mainScreen].bounds.size.height
```
