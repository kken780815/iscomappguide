# 類別

在類別的命名規則中我們單字的首字都大寫，後面再加上底線連接型別方便辨識，如下範例

```objectivec
@interface Shared_VC : MViewController //共乘的畫面
@end
```

{% hint style="info" %}
名稱通常都用名詞組合而成，後方型別也可用縮寫，但請明確並且簡單扼要的命名，必要的話後面可加上註解。
{% endhint %}

### 存放方式

類別檔案的存放方式須依功能分層級存放，如下範例

![](<../.gitbook/assets/螢幕快照 2019-08-07 下午12.13.06.png>)

{% hint style="info" %}
資料夾名稱首字一律大寫，命名請明確並且簡單扼要。
{% endhint %}
