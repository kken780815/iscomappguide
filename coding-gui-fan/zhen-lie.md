# 集合型別

## 陣列

**單字首字小寫後方加上s表示複數**，如下範例

```objectivec
@interface Shopping_VC : MViewController<SwipeMenuViewDelegate>
@property (weak, nonatomic) NSArray *products;
@end
```

## 字典

**單字首字小寫後方加上dic**，如下範例

```objectivec
@interface Shopping_VC : MViewController<SwipeMenuViewDelegate>
@property (weak, nonatomic) NSDictionary *productDic;
@end
```
