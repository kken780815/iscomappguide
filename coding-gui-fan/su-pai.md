# 格式與縮排

### 格式

Codeing的格式，必須善用#mark分類程式碼劃分區域，並加以命名，通常最上層是view的生命迴圈區域，再來是各個代理事件、點擊事件、自定義方法等等。 如下範例

```objectivec
@implementation Login_VC
#pragma mark - ViewController LifeCycle
- (void)viewDidLoad {
    ...
}
-(void)viewWillAppear:(BOOL)animated{
    ...
}
-(void)viewWillDisappear:(BOOL)animated{
    ...
}

#pragma mark - UITextFieldDelegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    ...
}

#pragma mark - IBAction
- (IBAction)goLogin:(id)sender {
    ...
}
- (IBAction)backBtn:(id)sender {
    ...
}

- (IBAction)registBtn:(id)sender {
    ...
}

#pragma mark - Method
-(void) doMemberTokenAPI {
    ...
}

```

確實做好mark區分，我們就可以利用右方的區段總覽，快速切換個區域，如下圖

![](<../.gitbook/assets/螢幕快照 2019-08-07 下午3.54.15.png>)

### 縮排

程式碼的縮排必須保持工整，不可過於雜亂，開發者本身務必保持程式碼的可讀性，方便未來或者其他同仁接手時，能快速上手。

```objectivec
//定位失敗
-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    if ([error code]==kCLErrorDenied) {
        NSLog(@"拒絕定位");
        isLocationOpen = false;
    }
    if ([error code]==kCLErrorLocationUnknown) {
        NSLog(@"無法獲得資訊");
        isLocationOpen = false;
    }
}
```

{% hint style="info" %}
&#x20;必要時可以加入註解，也可顯示Log，但是注意Log也必須保持工整明確，避免程式執行時一堆雜亂的Log出現。
{% endhint %}

