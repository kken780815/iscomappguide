# 各專案連結

**4g** - ssh://user@10.10.21.95/var/git/FourG/IOS/FourG.git

**宜蘭勁趣味** - ssh://user@10.10.21.95/var/git/Ilan\_Culture/Ilan\_Culture.git

**ipolice** - ssh://user@10.10.21.95/var/git/Ipolice/ios/Ipolice.git

**宜蘭學習地圖** - ssh://user@10.10.21.95/var/git/Ilan\_Edu/Ilan\_Edu.git

**張律師** - ssh://user@10.10.21.95/var/git/ICCSMP/ICCSMP

**順捷** - ssh://user@10.10.21.95/var/git/FJAPP.git

**苗栗舊山線** - ssh://user@10.10.21.95/var/git/Miaoli\_Line/Miaoli\_Line.git

**桃園地景節** - ssh://user@10.10.21.95/var/git//Taoyuan\_LAF/Taoyuan\_LAF.git

**嘉嘉** - ssh://user@10.10.21.95/var/git/Chiachia.git

**旅行台灣** - ssh://user@10.10.21.95/var/git/TourTaiwan.git

**石門水庫** - ssh://user@10.10.21.95/var/git/Shimen.git

**南投智慧生活** - ssh://user@10.10.21.95/var/git/Nantou\_life.git

**4g+** - ssh://user@10.10.21.95/var/git/FourG+.git

**myCTstore** - ssh://user@10.10.21.95/var/git/myCTstore.git

**桃園整合App** - ssh://user@10.10.21.95/var/git/TaoyuanUnite.git

**桃園農博** - ssh://user@10.10.21.95/var/git/TaoyuanAgroFair.git

**東協** - ssh://user@10.10.21.95/var/git/ASEAN.git

**BeaconDemo** - ssh://user@10.10.21.95/var/git/BeaconDemo.git

**FlayGoApp** - ssh://user@10.10.21.95/var/git/FlyGoApp.git

**宜蘭學習地圖** - ssh://user@10.10.21.95/var/git/IlanLBS.git

**Concord** - ssh://user@10.10.21.95/var/git/Concord.git

**智慧城鄉** - ssh://user@10.10.21.95/var/git/SmartTaipei.git

**台中經濟平台(購物節)** - ssh://user@10.10.21.95/var/git/Funtaichung.git

**EzTime(西艾迪)** - ssh://user@10.10.21.95/var/git/EzTime.git
