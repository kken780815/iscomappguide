# Table of contents

* [前言](README.md)

## Coding規範

* [變數](coding-gui-fan/untitled.md)
* [常數](coding-gui-fan/chang-shu.md)
* [方法](coding-gui-fan/fang-fa.md)
* [集合型別](coding-gui-fan/zhen-lie.md)
* [類別](coding-gui-fan/lei-bie.md)
* [圖片](coding-gui-fan/tu-pian.md)
* [定義](coding-gui-fan/ding-yi.md)
* [格式與縮排](coding-gui-fan/su-pai.md)
* [介面元件的宣告](coding-gui-fan/jie-mian-yuan-jian-de-xuan-gao.md)

## 版本控制 <a href="#sourcetree-shi-yong-gui-fan" id="sourcetree-shi-yong-gui-fan"></a>

* [SourceTree](sourcetree-shi-yong-gui-fan/untitled.md)
* [各專案連結](sourcetree-shi-yong-gui-fan/git-url.md)

## 小技巧

* [黃色小鴨除錯法](xiao-ji-qiao/huang-se-xiao-ya-chu-cuo-fa.md)
